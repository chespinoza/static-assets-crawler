package main

import (
	"flag"
	"fmt"
	"io"
	"log"
	"net/http"
	"sort"
	"strings"

	"golang.org/x/net/html"
)

func main() {
	url := flag.String("url", "https://www.google.com", "url to crawl")
	flag.Parse()
	fmt.Printf("crawling URL:%s\n", *url)

	body, err := Request(*url)
	if err != nil {
		log.Fatalln("Error:" + err.Error())
	}
	defer body.Close()

	links, err := Parse(body,
		func(token_data string) bool {
			if token_data == "a" {
				return true
			}
			return false
		},
		func(attr html.Attribute) bool {
			if attr.Key == "href" && strings.HasPrefix(attr.Val, "/") {
				return true
			}
			return false
		},
	)
	if err != nil {
		log.Fatalln("Error:" + err.Error())
	}

	for _, link := range links {
		body, err := Request(*url + link)
		if err == nil {
			fmt.Printf("Resource [%s]\n", link)
			fmt.Println("\tStatic Assets:")
			assets, err := Parse(body,
				func(token_data string) bool {
					if token_data == "link" || token_data == "script" || token_data == "img" {
						return true
					}
					return false
				},
				func(attr html.Attribute) bool {
					if (attr.Key == "src" || attr.Key == "href") && attr.Val != "" {
						return true
					}
					return false
				})
			if err == nil {
				for _, asset := range assets {
					fmt.Println("\t->", asset)
				}
			}

			fmt.Println()
			body.Close()
		}
	}

}

// Request fetches an <url> and returns a io.ReadCloser with the html document body
// if there are no errors.
func Request(url string) (io.ReadCloser, error) {
	response, err := http.Get(url)
	if err != nil {
		return nil, err
	}
	if response.StatusCode != http.StatusOK {
		return nil, fmt.Errorf("Error, Response Status Code:%d", response.StatusCode)
	}
	return response.Body, nil
}

// Parse reads a html Body, parses it and checks for token's data and their attributes which
// will define what to <items> to collect
func Parse(body io.ReadCloser, checkData func(string) bool, checkAttributes func(html.Attribute) bool) (items []string, err error) {
	tokenizer := html.NewTokenizer(body)
	for {
		token_pos := tokenizer.Next()
		switch token_pos {
		case html.ErrorToken:
			if tokenizer.Err() == io.EOF {
				items = removeDuplicates(items)
				sort.Strings(items)
				return
			}
			return nil, fmt.Errorf("error running tokenizer: %s", tokenizer.Err())

		case html.StartTagToken, html.EndTagToken, html.SelfClosingTagToken:
			token := tokenizer.Token()
			if checkData(token.Data) {
				for _, attr := range token.Attr {
					if checkAttributes(attr) {
						items = append(items, attr.Val)
					}
				}
			}
		}
	}
}

// removeDuplicates removes duplicates strings from a slice of strings
func removeDuplicates(strs []string) []string {
	uniques := make(map[string]struct{})
	var uniques_strs []string
	for _, val := range strs {
		uniques[val] = struct{}{}
	}
	for val, _ := range uniques {
		uniques_strs = append(uniques_strs, val)
	}
	return uniques_strs
}
