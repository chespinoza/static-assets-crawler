# Static Assets Crawler

### how to run it using the predetermined url 
```
go run main.go
```
### how to build it
```
go build
```
### how to use the executable
```
./crawler -url=http://google.com

crawling URL:http://google.com
Resource [/advanced_search?hl=en-GB&authuser=0]
	Static Assets:

Resource [/intl/en/about.html]
	Static Assets:
	-> //ajax.googleapis.com/ajax/libs/angularjs/1.6.6/angular-animate.min.js
	-> //ajax.googleapis.com/ajax/libs/angularjs/1.6.6/angular-sanitize.min.js
	-> //ajax.googleapis.com/ajax/libs/angularjs/1.6.6/angular-touch.min.js
	-> //ajax.googleapis.com/ajax/libs/angularjs/1.6.6/angular.min.js
	-> //fonts.googleapis.com/css?family=Roboto:100,300,400,500,700|Google+Sans:400,500,700,900|Google+Sans+Display:400,500
	-> //www.google.com/jsapi
	-> //www.gstatic.com/external_hosted/gsap/v1_18_0/TweenMax.min.js
	-> //www.gstatic.com/external_hosted/gsap/v1_18_0/plugins/ScrollToPlugin.min.js
	-> //www.gstatic.com/external_hosted/intersectionobserver_polyfill/intersection-observer.min.js
	-> //www.gstatic.com/external_hosted/ng_ui_router/release/angular-ui-router.min.js
	-> //www.gstatic.com/external_hosted/picturefill/picturefill.min.js
	-> //www.gstatic.com/external_hosted/pixi/pixi.min.js
	-> //www.gstatic.com/external_hosted/scrollmagic/ScrollMagic.min.js
	-> //www.gstatic.com/external_hosted/scrollmagic/animation.gsap.min.js
	-> //www.gstatic.com/feedback/js/help/prod/service/lazy.min.js
	-> /assets/css/main.min.css?cache=9c20ddb
	-> /assets/img/close.png?cache=5628868
	-> /assets/img/keyboard-arrow-up-999.svg?cache=911336e
	-> /assets/img/menu.png?cache=38c6aed
	-> /assets/js/detect.min.js?cache=c84f19a
	-> /assets/js/main.min.js?cache=579f8b8
	-> https://about.google/
	-> https://www.gstatic.com/images/branding/googleg/2x/googleg_standard_color_120dp.png

Resource [/intl/en/ads/]
	Static Assets:
	-> //ajax.googleapis.com
	-> //fonts.googleapis.com/css?family=Roboto:200,300,400,500,700|Google+Sans:400,500|Product+Sans:400&lang=en&display=swap
	-> //www.google-analytics.com
	-> //www.google.com
	-> //www.google.com/images/branding/product/1x/ads_24dp.png
	-> //www.googleadservices.com
	-> //www.googletagmanager.com
	-> //www.gstatic.com
	-> //www.gstatic.com/external_hosted/picturefill/picturefill.min.js
	-> /home/static/home/css/index.min.css?cache=2c138a7
	-> /home/static/home/js/detect.min.js?cache=1f7ea63
	-> /home/static/home/js/index.min.js?cache=0eb3f9c
	-> /home/static/js/bs-analytics.min.js?cache=df426cb
	-> /home/static/js/butterbar/butterbar.min.js?cache=e789d5b
	-> data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAEAAAABCAQAAAC1HAwCAAAAC0lEQVR42mNkYAAAAAYAAjCB0C8AAAAASUVORK5CYII=
	-> https://ads.google.com/home/
	-> https://ajax.googleapis.com/ajax/libs/angularjs/1.6.4/angular-animate.min.js
	-> https://ajax.googleapis.com/ajax/libs/angularjs/1.6.4/angular-touch.min.js
	-> https://ajax.googleapis.com/ajax/libs/angularjs/1.6.4/angular.min.js
	-> https://www.gstatic.com/external_hosted/gsap/TweenMax.min.js
	-> https://www.gstatic.com/external_hosted/scrollmagic/ScrollMagic.min.js
	-> https://www.gstatic.com/external_hosted/scrollmagic/animation.gsap.min.js

Resource [/intl/en/policies/privacy/]
	Static Assets:

Resource [/intl/en/policies/terms/]
	Static Assets:

Resource [/preferences?hl=en]
	Static Assets:
	-> /images/warning.gif

Resource [/services/]
	Static Assets:
	-> ../services/css/business-solutions.min.css
	-> ../services/images/cloud_logo_lockup_1x.png
	-> ../services/images/hero-bg_m_1x.png
	-> ../services/images/section-advertise-card-img_1x.jpg
	-> ../services/images/section-earn-card-img_1x.jpg
	-> ../services/js/business-solutions.min.js
	-> //ajax.googleapis.com/ajax/libs/angularjs/1.5.9/angular-animate.min.js
	-> //ajax.googleapis.com/ajax/libs/angularjs/1.5.9/angular-messages.min.js
	-> //ajax.googleapis.com/ajax/libs/angularjs/1.5.9/angular.min.js
	-> //fonts.googleapis.com/css?family=Product+Sans:400&lang=en
	-> //fonts.googleapis.com/css?family=Roboto+Slab:300,400,500&lang=en
	-> //fonts.googleapis.com/css?family=Roboto:300,400,500,700&lang=en
	-> //www.google.co.uk/ads/images/icons/analytics-new_1x.png
	-> //www.google.co.uk/ads/images/icons/datastudio_1x.png
	-> //www.google.co.uk/ads/images/icons/icon-arrow-down.png
	-> //www.google.co.uk/ads/images/icons/icon-arrow-related.png
	-> //www.google.co.uk/ads/images/icons/icon-fab-scroll.png
	-> //www.google.co.uk/ads/images/icons/icon-fab-video.png
	-> //www.google.co.uk/ads/images/icons/icon-footer-blogger.png
	-> //www.google.co.uk/ads/images/icons/icon-footer-help.png
	-> //www.google.co.uk/ads/images/icons/icon-footer-twitter.png
	-> //www.google.co.uk/ads/images/icons/icon-footer-youtube.png
	-> //www.google.co.uk/ads/images/icons/icon-product-admob-new-64.png
	-> //www.google.co.uk/ads/images/icons/icon-product-ads-new-64.png
	-> //www.google.co.uk/ads/images/icons/icon-product-adsense-new-64.png
	-> //www.google.co.uk/ads/images/icons/icon-ui-phone.png
	-> //www.google.co.uk/images/branding/product/1x/hh_calendar_64dp.png
	-> //www.google.co.uk/images/branding/product/1x/hh_docs_64dp.png
	-> //www.google.co.uk/images/branding/product/1x/hh_drive_64dp.png
	-> //www.google.co.uk/images/branding/product/1x/hh_gmail_64dp.png
	-> //www.google.co.uk/images/branding/product/1x/youtube_64dp.png
	-> //www.google.co.uk/images/icons/material/product/1x/shopping_64dp.png
	-> //www.google.co.uk/images/icons/material/system/1x/smartphone_black_36dp.png
	-> //www.google.co.uk/images/icons/product/cloud_platform-64.png
	-> //www.google.com/ads/js/peitho2/peitho2.min.js
	-> //www.gstatic.com/external_hosted/modernizr/v2_8_3/basic.js
	-> //www.gstatic.com/external_hosted/picturefill/picturefill.min.js
	-> data:image/gif;base64,R0lGODlhAQABAIAAAAAAAP///yH5BAEAAAAALAAAAAABAAEAAAIBRAA7
	-> https://www.google.com/images/branding/googlelogo/1x/googlelogo_color_116x41dp.png
	-> https://www.google.com/images/branding/product/ico/googleg_alldp.ico
	-> https://www.google.com/intl/en_uk/services/
```
